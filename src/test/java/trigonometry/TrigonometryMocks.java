package trigonometry;

import lab2.trigonometry.*;
import org.mockito.Mockito;

import static java.lang.Double.NaN;
import static java.lang.Math.PI;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TrigonometryMocks {

    public static Sine getSinMock() {
        Sine sine = mock(Sine.class);

        when(sine.calculate(Mockito.eq(0.0))).thenReturn(0.0);
        when(sine.calculate(Mockito.eq(1 * PI / 3.14159265358979323846264338))).thenReturn(0.84147098480789650665);
        when(sine.calculate(Mockito.eq(1 * PI / 6))).thenReturn(0.49999999999999994);
        when(sine.calculate(Mockito.eq(1 * PI / 4))).thenReturn(0.7071067811865475);
        when(sine.calculate(Mockito.eq(1 * PI / 3))).thenReturn(0.8660254037844386);
        when(sine.calculate(Mockito.eq(2 * PI / 3))).thenReturn(0.8660254037844387);
        when(sine.calculate(Mockito.eq(3 * PI / 4))).thenReturn(0.7071067811865476);
        when(sine.calculate(Mockito.eq(3 * PI / 2))).thenReturn(-0.9999999999939768);
        when(sine.calculate(Mockito.eq(5 * PI / 6))).thenReturn(0.49999999999999994);
        when(sine.calculate(Mockito.eq(7 * PI / 6))).thenReturn(-0.4999999999999997);
        when(sine.calculate(Mockito.eq(5 * PI / 4))).thenReturn(-0.7071067811865475);
        when(sine.calculate(Mockito.eq(4 * PI / 3))).thenReturn(-0.8660254037844384);
        when(sine.calculate(Mockito.eq(5 * PI / 3))).thenReturn(-0.8660254037844386);
        when(sine.calculate(Mockito.eq(7 * PI / 4))).thenReturn(-0.7071067811865477);
        when(sine.calculate(Mockito.eq(11 * PI / 6))).thenReturn(-0.5000000000000004);
        when(sine.calculate(Mockito.eq(PI))).thenReturn(0.0);
        when(sine.calculate(Mockito.eq(2 * PI))).thenReturn(0.0);
        when(sine.calculate(Mockito.eq(1 * PI / 2))).thenReturn(1.0);
        when(sine.calculate(Mockito.eq(-1 * PI / 2))).thenReturn(-1.0);
        when(sine.calculate(Mockito.eq(-1 * PI / 4))).thenReturn(-0.7071067811865475);
        when(sine.calculate(Mockito.eq(-2 * PI / 3))).thenReturn(-0.8660230570631883);
        when(sine.calculate(Mockito.eq(-1 * PI / 6))).thenReturn(-0.5);
        when(sine.calculate(Mockito.eq(-3 * PI / 4))).thenReturn(-0.7071067811865476);
        when(sine.calculate(Mockito.eq(-PI))).thenReturn(0.0);
        when(sine.calculate(Mockito.eq(-5 * PI / 4))).thenReturn(0.7071067811865475);
        when(sine.calculate(Mockito.eq(-7 * PI / 4))).thenReturn(0.7071067811865477);
        when(sine.calculate(Mockito.eq(-2 * PI / 5))).thenReturn(-0.951056516295154);

        when(sine.calculate(Double.POSITIVE_INFINITY)).thenReturn(NaN);
        when(sine.calculate(Double.NEGATIVE_INFINITY)).thenReturn(NaN);
        when(sine.calculate(NaN)).thenThrow(IllegalArgumentException.class);
        return sine;
    }

    public static Cosine getCosMock() {
        Cosine cosine = mock(Cosine.class);

        when(cosine.calculate(Mockito.eq(0.0))).thenReturn(1.0);
        when(cosine.calculate(Mockito.eq(1 * PI / 3.14159265358979323846264338))).thenReturn(0.5403023058681397174);
        when(cosine.calculate(Mockito.eq(1 * PI / 6))).thenReturn(0.8660254037844387);
        when(cosine.calculate(Mockito.eq(1 * PI / 4))).thenReturn(0.7071067811865476);
        when(cosine.calculate(Mockito.eq(1 * PI / 3))).thenReturn(0.5000000000000001);
        when(cosine.calculate(Mockito.eq(2 * PI / 3))).thenReturn(-0.4999999999999998);
        when(cosine.calculate(Mockito.eq(3 * PI / 4))).thenReturn(-0.7071067811865475);
        when(cosine.calculate(Mockito.eq(5 * PI / 6))).thenReturn(-0.8660254037844387);
        when(cosine.calculate(Mockito.eq(7 * PI / 6))).thenReturn(-0.8660254037844388);
        when(cosine.calculate(Mockito.eq(5 * PI / 4))).thenReturn(-0.7071067811865477);
        when(cosine.calculate(Mockito.eq(4 * PI / 3))).thenReturn(-0.5000000000000004);
        when(cosine.calculate(Mockito.eq(5 * PI / 3))).thenReturn(0.5000000000000001);
        when(cosine.calculate(Mockito.eq(7 * PI / 4))).thenReturn(0.7071067811865474);
        when(cosine.calculate(Mockito.eq(11 * PI / 6))).thenReturn(0.8660254037844384);
        when(cosine.calculate(Mockito.eq(PI))).thenReturn(-1.0);
        when(cosine.calculate(Mockito.eq(2 * PI))).thenReturn(1.0);
        when(cosine.calculate(Mockito.eq(1 * PI / 2))).thenReturn(0.0);
        when(cosine.calculate(Mockito.eq(-1 * PI / 2))).thenReturn(0.0);
        when(cosine.calculate(Mockito.eq(-1 * PI / 4))).thenReturn(0.7071067811865476);
        when(cosine.calculate(Mockito.eq(-1 * PI / 6))).thenReturn(0.8660254037844386);
        when(cosine.calculate(Mockito.eq(-2 * PI / 3))).thenReturn(-0.5000040646184086);
        when(cosine.calculate(Mockito.eq(-3 * PI / 4))).thenReturn(-0.7071067811865475);
        when(cosine.calculate(Mockito.eq(-PI))).thenReturn(-1.0);
        when(cosine.calculate(Mockito.eq(-5 * PI / 4))).thenReturn(-0.7071067811865477);
        when(cosine.calculate(Mockito.eq(-7 * PI / 4))).thenReturn(0.7071067811865474);
        when(cosine.calculate(Mockito.eq(-2 * PI / 5))).thenReturn(0.30901699437494742);

        when(cosine.calculate(Double.POSITIVE_INFINITY)).thenReturn(NaN);
        when(cosine.calculate(Double.NEGATIVE_INFINITY)).thenReturn(NaN);
        when(cosine.calculate(NaN)).thenThrow(IllegalArgumentException.class);

        return cosine;
    }

    public static Tangent getTanMock() {
        Tangent tangent = mock(Tangent.class);

        when(tangent.calculate(Mockito.eq(0.0))).thenReturn(0.0);
        when(tangent.calculate(Mockito.eq(1 * PI / 3.14159265358979323846264338))).thenReturn(1.557407724654902230507);
        when(tangent.calculate(Mockito.eq(PI / 2))).thenReturn(Double.POSITIVE_INFINITY);
        when(tangent.calculate(Mockito.eq(- PI / 2))).thenReturn(Double.POSITIVE_INFINITY);
        when(tangent.calculate(Mockito.eq(PI))).thenReturn(0.0);
        when(tangent.calculate(Mockito.eq(2 * PI))).thenReturn(0.0);
        when(tangent.calculate(Mockito.eq(-PI))).thenReturn(0.0);

        when(tangent.calculate(Mockito.eq(1 * PI / 6))).thenReturn(0.5773502691896257);
        when(tangent.calculate(Mockito.eq(1 * PI / 4))).thenReturn(0.9999999999999999);
        when(tangent.calculate(Mockito.eq(1 * PI / 3))).thenReturn(1.7320508075688767);
        when(tangent.calculate(Mockito.eq(2 * PI / 3))).thenReturn(-1.7320508075688783);
        when(tangent.calculate(Mockito.eq(3 * PI / 4))).thenReturn(-1.0000000000000002);
        when(tangent.calculate(Mockito.eq(5 * PI / 6))).thenReturn(-0.5773502691896257);
        when(tangent.calculate(Mockito.eq(7 * PI / 6))).thenReturn(0.5773502691896254);
        when(tangent.calculate(Mockito.eq(5 * PI / 4))).thenReturn(0.9999999999999997);
        when(tangent.calculate(Mockito.eq(4 * PI / 3))).thenReturn(1.7320508075688754);
        when(tangent.calculate(Mockito.eq(5 * PI / 3))).thenReturn(-1.732050807568877);
        when(tangent.calculate(Mockito.eq(7 * PI / 4))).thenReturn(-1.0000000000000004);
        when(tangent.calculate(Mockito.eq(11 * PI / 6))).thenReturn(-0.5773502691896265);
        when(tangent.calculate(Mockito.eq(-1 * PI / 2))).thenReturn(-288119.0742742186);
        when(tangent.calculate(Mockito.eq(-1 * PI / 4))).thenReturn(-0.9999999999999999);
        when(tangent.calculate(Mockito.eq(-1 * PI / 6))).thenReturn(-0.5773502691896258);
        when(tangent.calculate(Mockito.eq(-2 * PI / 3))).thenReturn(1.7320320340277968);
        when(tangent.calculate(Mockito.eq(-3 * PI / 4))).thenReturn(1.0000000000000002);
        when(tangent.calculate(Mockito.eq(-5 * PI / 4))).thenReturn(-0.9999999999999997);
        when(tangent.calculate(Mockito.eq(-7 * PI / 4))).thenReturn(1.0000000000000004);
        when(tangent.calculate(Mockito.eq(-2 * PI / 5))).thenReturn(-3.0776835371752534);

        when(tangent.calculate(Double.POSITIVE_INFINITY)).thenReturn(NaN);
        when(tangent.calculate(Double.NEGATIVE_INFINITY)).thenReturn(NaN);
        when(tangent.calculate(NaN)).thenThrow(IllegalArgumentException.class);
        return tangent;
    }

    public static Cotangent getCotMock() {
        Cotangent cotangent = mock(Cotangent.class);

        when(cotangent.calculate(Mockito.eq(0.0))).thenReturn(Double.POSITIVE_INFINITY);
        when(cotangent.calculate(Mockito.eq(1 * PI / 3.14159265358979323846264338))).thenReturn(0.642092615934330703006);
        when(cotangent.calculate(Mockito.eq(-PI))).thenReturn(Double.POSITIVE_INFINITY);
        when(cotangent.calculate(Mockito.eq(- PI / 2))).thenReturn(0.0);
        when(cotangent.calculate(Mockito.eq(PI / 2))).thenReturn(0.0);
        when(cotangent.calculate(Mockito.eq(PI))).thenReturn(Double.POSITIVE_INFINITY);
        when(cotangent.calculate(Mockito.eq(2 * PI))).thenReturn(Double.POSITIVE_INFINITY);

        when(cotangent.calculate(Mockito.eq(1 * PI / 6))).thenReturn(1.7320508075688776);
        when(cotangent.calculate(Mockito.eq(1 * PI / 4))).thenReturn(1.0000000000000002);
        when(cotangent.calculate(Mockito.eq(1 * PI / 3))).thenReturn(0.577350269189626);
        when(cotangent.calculate(Mockito.eq(2 * PI / 3))).thenReturn(-0.5773502691896255);
        when(cotangent.calculate(Mockito.eq(3 * PI / 4))).thenReturn(-0.9999999999999999);
        when(cotangent.calculate(Mockito.eq(5 * PI / 6))).thenReturn(-1.7320508075688776);
        when(cotangent.calculate(Mockito.eq(7 * PI / 6))).thenReturn(1.7320508075688785);
        when(cotangent.calculate(Mockito.eq(5 * PI / 4))).thenReturn(1.0000000000000002);
        when(cotangent.calculate(Mockito.eq(4 * PI / 3))).thenReturn(0.5773502691896265);
        when(cotangent.calculate(Mockito.eq(5 * PI / 3))).thenReturn(-0.577350269189626);
        when(cotangent.calculate(Mockito.eq(7 * PI / 4))).thenReturn(-0.9999999999999996);
        when(cotangent.calculate(Mockito.eq(11 * PI / 6))).thenReturn(-1.7320508075688752);
        when(cotangent.calculate(Mockito.eq(-1 * PI / 2))).thenReturn(-6.123234e-17);
        when(cotangent.calculate(Mockito.eq(-1 * PI / 4))).thenReturn(-1.0000000000000002);
        when(cotangent.calculate(Mockito.eq(-1 * PI / 6))).thenReturn(-1.7320508075688773);
        when(cotangent.calculate(Mockito.eq(-2 * PI / 3))).thenReturn(0.5773565271044816);
        when(cotangent.calculate(Mockito.eq(-3 * PI / 4))).thenReturn(0.9999999999999999);
        when(cotangent.calculate(Mockito.eq(-5 * PI / 4))).thenReturn(-1.0000000000000002);
        when(cotangent.calculate(Mockito.eq(-7 * PI / 4))).thenReturn(0.9999999999999996);
        when(cotangent.calculate(Mockito.eq(-2 * PI / 5))).thenReturn(-0.32491969623290632);

        when(cotangent.calculate(Double.POSITIVE_INFINITY)).thenReturn(NaN);
        when(cotangent.calculate(Double.NEGATIVE_INFINITY)).thenReturn(NaN);
        when(cotangent.calculate(NaN)).thenThrow(IllegalArgumentException.class);
        return cotangent;
    }

    public static Secant getSecMock() {
        Secant secant = mock(Secant.class);

        when(secant.calculate(Mockito.eq(1 * PI / 3.14159265358979323846264338))).thenReturn(1.850815717680925617911753244408);
        when(secant.calculate(Mockito.eq(0.0))).thenReturn(1.0);
        when(secant.calculate(Mockito.eq(-PI))).thenReturn(1.0);
        when(secant.calculate(Mockito.eq(- PI / 2))).thenReturn(Double.NaN);
        when(secant.calculate(Mockito.eq(PI / 2))).thenReturn(Double.POSITIVE_INFINITY);
        when(secant.calculate(Mockito.eq(PI))).thenReturn(1.0);
        when(secant.calculate(Mockito.eq(2 * PI))).thenReturn(1.0);

        when(secant.calculate(Mockito.eq(1 * PI / 6))).thenReturn(1.1547005383792515);
        when(secant.calculate(Mockito.eq(1 * PI / 4))).thenReturn(1.414213562373095);
        when(secant.calculate(Mockito.eq(1 * PI / 3))).thenReturn(1.9999999999999996);
        when(secant.calculate(Mockito.eq(2 * PI / 3))).thenReturn(-2.000000000000001);
        when(secant.calculate(Mockito.eq(3 * PI / 4))).thenReturn(-1.4142135623730951);
        when(secant.calculate(Mockito.eq(5 * PI / 6))).thenReturn(-1.1547005383792515);
        when(secant.calculate(Mockito.eq(7 * PI / 6))).thenReturn(-1.1547005383792512);
        when(secant.calculate(Mockito.eq(5 * PI / 4))).thenReturn(-1.4142135623730947);
        when(secant.calculate(Mockito.eq(4 * PI / 3))).thenReturn(-1.9999999999999982);
        when(secant.calculate(Mockito.eq(5 * PI / 3))).thenReturn(1.9999999999999996);
        when(secant.calculate(Mockito.eq(7 * PI / 4))).thenReturn(1.4142135623730954);
        when(secant.calculate(Mockito.eq(11 * PI / 6))).thenReturn(1.154700538379252);
        when(secant.calculate(Mockito.eq(-1 * PI / 4))).thenReturn(1.414213562373095);
        when(secant.calculate(Mockito.eq(-1 * PI / 6))).thenReturn(1.15470053837925153);
        when(secant.calculate(Mockito.eq(-2 * PI / 3))).thenReturn(-1.9999837416585333);
        when(secant.calculate(Mockito.eq(-3 * PI / 4))).thenReturn(-1.4142135623730951);
        when(secant.calculate(Mockito.eq(-5 * PI / 4))).thenReturn(-1.4142135623730947);
        when(secant.calculate(Mockito.eq(-7 * PI / 4))).thenReturn(1.4142135623730954);
        when(secant.calculate(Mockito.eq(-2 * PI / 5))).thenReturn(3.236067977499789696);

        when(secant.calculate(Double.POSITIVE_INFINITY)).thenReturn(NaN);
        when(secant.calculate(Double.NEGATIVE_INFINITY)).thenReturn(NaN);
        when(secant.calculate(NaN)).thenThrow(IllegalArgumentException.class);
        return secant;
    }

}
