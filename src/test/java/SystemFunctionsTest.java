import lab2.SystemFunctions;
import lab2.log.NaturalLogarithm;
import lab2.log.LogarithmBaseN;
import lab2.trigonometry.*;
import log.LogarithmMocks;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import trigonometry.TrigonometryMocks;

import static java.lang.Math.PI;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(value=PER_CLASS)
public class SystemFunctionsTest {
    private static final double DELTA = 0.05;
    private static final double ACCURACY = 0.001;
    private SystemFunctions function;
    private Sine sine;
    private Cosine cosine;
    private Cotangent cotangent;
    private Secant secant;
    private Tangent tangent;
    private NaturalLogarithm naturalLogarithm;
    private LogarithmBaseN log2;
    private LogarithmBaseN log3;
    private LogarithmBaseN log5;
    private LogarithmBaseN log10;


    @ParameterizedTest
    @CsvFileSource(resources = "/system_function_test_data.csv")
    public void SystemWithAllStubsTest(double expected, double num, double den) {
        function = new SystemFunctions(ACCURACY, TrigonometryMocks.getCosMock(), TrigonometryMocks.getCotMock(),
                TrigonometryMocks.getSecMock(), TrigonometryMocks.getTanMock(), TrigonometryMocks.getSinMock(), LogarithmMocks.getLnMock(), LogarithmMocks.getLog2Mock(),
                LogarithmMocks.getLog3Mock(), LogarithmMocks.getLog5Mock(), LogarithmMocks.getLog10Mock());
        double result = function.calculate(num * PI / den);
        Assertions.assertEquals(expected, result, DELTA);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/system_function_test_data.csv")
    public void SystemWithBasicStubsTest(double expected, double num, double den) {
        cosine = new Cosine(ACCURACY, TrigonometryMocks.getSinMock());
        cotangent = new Cotangent(ACCURACY, TrigonometryMocks.getSinMock(), cosine);
        secant = new Secant(ACCURACY, cosine);
        tangent = new Tangent(ACCURACY, TrigonometryMocks.getSinMock(), cosine);
        sine = TrigonometryMocks.getSinMock();
        log2 = new LogarithmBaseN(ACCURACY, 2, LogarithmMocks.getLnMock());
        log3 = new LogarithmBaseN(ACCURACY, 3, LogarithmMocks.getLnMock());
        log5 = new LogarithmBaseN(ACCURACY, 5, LogarithmMocks.getLnMock());
        log10 = new LogarithmBaseN(ACCURACY, 10, LogarithmMocks.getLnMock());

        function = new SystemFunctions(ACCURACY, cosine, cotangent, secant,
                tangent, sine, LogarithmMocks.getLnMock(), log2,
                log3, log5, log10);
        double result = function.calculate(num * PI / den);
        Assertions.assertEquals(expected, result, DELTA);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/system_function_test_data.csv")
    public void SystemWithoutStubsTest(double expected, double num, double den) {
        sine = new Sine(ACCURACY);
        cosine = new Cosine(ACCURACY, sine);
        cotangent = new Cotangent(ACCURACY, sine, cosine);
        secant = new Secant(ACCURACY, cosine);
        tangent = new Tangent(ACCURACY, sine, cosine);
        naturalLogarithm = new NaturalLogarithm(ACCURACY);
        log2 = new LogarithmBaseN(ACCURACY, 2, naturalLogarithm);
        log3 = new LogarithmBaseN(ACCURACY, 3, naturalLogarithm);
        log5 = new LogarithmBaseN(ACCURACY, 5, naturalLogarithm);
        log10 = new LogarithmBaseN(ACCURACY, 10, naturalLogarithm);

        function = new SystemFunctions(ACCURACY, cosine, cotangent,
                secant, tangent, sine, naturalLogarithm, log2,
                log3, log5, log10);
        double result = function.calculate(num * PI / den);
        Assertions.assertEquals(expected, result, DELTA);
    }
}
