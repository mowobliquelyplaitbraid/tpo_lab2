package lab2;

import lab2.log.NaturalLogarithm;
import lab2.log.LogarithmBaseN;
import lab2.trigonometry.*;

import static java.lang.Math.PI;

public class Main {
    public static void main(String[] args) {
        func(10*PI);
    }

    static void func (double x) {
        NaturalLogarithm naturalLogarithm = new NaturalLogarithm(0.0001);
        LogarithmBaseN log2 = new LogarithmBaseN(0.0001, 2, naturalLogarithm);
        LogarithmBaseN log3 = new LogarithmBaseN(0.0001, 3, naturalLogarithm);
        LogarithmBaseN log5 = new LogarithmBaseN(0.0001, 5, naturalLogarithm);
        LogarithmBaseN log10 = new LogarithmBaseN(0.0001, 10, naturalLogarithm);
        Sine sine = new Sine(0.0001);
        Cosine cosine = new Cosine(0.0001, sine);
        Secant secant = new Secant(0.0001, cosine);
        Cotangent cotangent = new Cotangent(0.0001, sine, cosine);
        Tangent tangent = new Tangent(0.0001, sine, cosine);
        SystemFunctions func = new SystemFunctions(0.0001, cosine, cotangent, secant, tangent, sine, naturalLogarithm, log2, log3, log5, log10);

        CsvOut csvLogger1 = new CsvOut("out/cot.csv", -10, -0.1, 0.2);
        csvLogger1.log(cotangent);

        CsvOut csvLogger2 = new CsvOut("out/cos.csv", -10, -0.1, 0.2);
        csvLogger2.log(cosine);

        CsvOut csvLogger3 = new CsvOut("out/sin.csv", -10, -0.1, 0.2);
        csvLogger3.log(sine);

        CsvOut csvLogger4 = new CsvOut("out/sec.csv", -10, -0.1, 0.2);
        csvLogger4.log(secant);

        CsvOut csvLogger5 = new CsvOut("out/tan.csv", -10, -0.1, 0.2);
        csvLogger5.log(tangent);

        CsvOut csvLogger6 = new CsvOut("out/ln.csv", 0.1, 10, 0.2);
        csvLogger6.log(naturalLogarithm);

        CsvOut csvLogger7 = new CsvOut("out/log2.csv", 0.1, 10, 0.2);
        csvLogger7.log(log2);

        CsvOut csvLogger8 = new CsvOut("out/log3.csv", 0.1, 10, 0.2);
        csvLogger8.log(log3);

        CsvOut csvLogger9 = new CsvOut("out/log5.csv", 0.1, 10, 0.2);
        csvLogger9.log(log5);

        CsvOut csvLogger11 = new CsvOut("out/log10.csv", 0.1, 10, 0.2);
        csvLogger11.log(log10);

        CsvOut csvLogger10 = new CsvOut("out/func.csv", -10, 10, 0.1);
        csvLogger10.log(func);
    }
}
