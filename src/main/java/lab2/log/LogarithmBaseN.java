package lab2.log;

import lab2.Function;

public class LogarithmBaseN extends Function {

    private NaturalLogarithm naturalLogarithm;
    private int base;

    public LogarithmBaseN(double accuracy, int base, NaturalLogarithm naturalLogarithm) {
        super(accuracy);
        this.base = base;
        this.naturalLogarithm = naturalLogarithm;
    }

    @Override
    public Double calculate(double x) {
        if (Double.isNaN(x) || x < 0.0) {
            return Double.NaN;
        } else if (x == Double.POSITIVE_INFINITY) {
            return Double.POSITIVE_INFINITY;
        } else if (x == 0.0) {
            return Double.NEGATIVE_INFINITY;
        }
        return naturalLogarithm.calculate(x) / naturalLogarithm.calculate(this.base);
    }
}

