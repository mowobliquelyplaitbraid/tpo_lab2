package lab2.trigonometry;

import lab2.Function;

public class Cosine extends Function {
    private Sine sine;

    public Cosine(double accuracy, Sine sine) {
        super(accuracy);
        this.sine = sine;
    }

    public Double calculate(double x) {
        if (Double.isNaN(x) || Double.isInfinite(x))
            return Double.NaN;
        double result = Math.sqrt(1 - Math.pow(sine.calculate(x), 2));
        double newX = x % (2 * Math.PI);
        if (newX > Math.PI / 2 && newX < 3 * Math.PI / 2 || newX < -Math.PI / 2 &&
                newX > -3 * Math.PI / 2) result = -result;
        return result;
    }
}