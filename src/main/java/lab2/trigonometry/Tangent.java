package lab2.trigonometry;

import lab2.Function;

public class Tangent extends Function {
    Sine sine;
    Cosine cosine;

    public Tangent(double accuracy, Sine sine, Cosine cosine) {
        super(accuracy);
        this.sine = sine;
        this.cosine = cosine;
    }

    @Override
    public Double calculate(double x) {
        if (Double.isNaN(x) || Double.isInfinite(x))
            return Double.NaN;
        return (sine.calculate(x) / cosine.calculate(x));
    }
}
