package lab2.trigonometry;

import lab2.Function;

public class Secant extends Function {
    Cosine cosine;

    public Secant(double accuracy, Cosine cosine) {
        super(accuracy);
        this.cosine = cosine;
    }

    @Override
    public Double calculate(double x) {
        if (Double.isNaN(x) || Double.isInfinite(x))
            return Double.NaN;
        return (1 / cosine.calculate(x));
    }
}
