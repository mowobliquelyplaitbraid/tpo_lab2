package lab2;
import lab2.trigonometry.*;
import lab2.log.*;

public class SystemFunctions extends Function {
    private Cosine cosine;
    private Cotangent cotangent;
    private Secant secant;
    private Tangent tangent;
    private Sine sine;
    private NaturalLogarithm naturalLogarithm;
    private LogarithmBaseN log2;
    private LogarithmBaseN log3;
    private LogarithmBaseN log5;
    private LogarithmBaseN log10;


    public SystemFunctions(double accuracy, Cosine cosine, Cotangent cotangent, Secant secant, Tangent tangent, Sine sine, NaturalLogarithm naturalLogarithm, LogarithmBaseN log2, LogarithmBaseN log3, LogarithmBaseN log5, LogarithmBaseN log10) {
        super(accuracy);
        this.cosine = cosine;
        this.cotangent = cotangent;
        this.secant = secant;
        this.tangent = tangent;
        this.sine = sine;
        this.naturalLogarithm = naturalLogarithm;
        this.log2 = log2;
        this.log3 = log3;
        this.log5 = log5;
        this.log10 = log10;
    }

    @Override
    public Double calculate(double x) {
        Double result;
        if (x <= 0) {
            result = ((((Math.pow(sine.calculate(x), 3) + cotangent.calculate(x)) / cosine.calculate(x)) * (secant.calculate(x) / sine.calculate(x))) / cosine.calculate(x));
        }
        else  {
            result = ((Math.pow(Math.pow(log3.calculate(x) * log2.calculate(x), 3), 3)) + (log10.calculate(x) +
                    ((log5.calculate(x) + log2.calculate(x)) / log10.calculate(x))) - log5.calculate(x));
        }
        if (Double.isInfinite(result)) return Double.NaN;
        else return result;
    }
}
